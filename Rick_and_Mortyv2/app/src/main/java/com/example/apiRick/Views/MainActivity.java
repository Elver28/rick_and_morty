package com.example.apiRick.Views;


import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.apiRick.Adapter.Adapter;
import com.example.apiRick.Interfaces.PresenterView;
import com.example.apiRick.Modelo.Rick;
import com.example.apiRick.Presenter.Presenter;
import com.example.apiRick.R;
import com.example.apiRick.Interfaces.MainActivityView;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MainActivityView {


    RecyclerView recyclerView;
    EditText buscarEdit;
    private PresenterView presenter = new Presenter(this, MainActivity.this);
    public Adapter tarjetaAdapter;
    ImageView buscar;


    //metodo para instanciar las variables
    @Override
    public void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = findViewById(R.id.recyclerView);

        buscar = findViewById(R.id.buscar);
        buscarEdit = findViewById(R.id.buscarEdit);

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               buscarTarjeta(buscarEdit.getText().toString());
                Toast.makeText(MainActivity.this, "Buscando...", Toast.LENGTH_SHORT).show();

            };
        });

        initView();

    }

    private void initView() {
        presenter.getTarjeta();
    }

    //metodo para mostrar las tarjetas
    @Override
    public void mostrarRick(ArrayList<Rick> tarjetasList) {

        tarjetaAdapter = new Adapter(tarjetasList, this);
        recyclerView.setAdapter(tarjetaAdapter);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        recyclerView.setLayoutManager(layoutManager);

    }

    @Override
    public void buscarTarjeta(String name) {
        presenter.buscarTarjeta(name);

    }
}