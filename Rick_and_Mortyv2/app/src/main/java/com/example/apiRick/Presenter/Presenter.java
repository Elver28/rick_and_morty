package com.example.apiRick.Presenter;

import android.content.Context;


import com.example.apiRick.Interfaces.InteractorView;
import com.example.apiRick.Interfaces.PresenterView;
import com.example.apiRick.Modelo.Interactor;
import com.example.apiRick.Modelo.Rick;
import com.example.apiRick.Interfaces.MainActivityView;
import java.util.ArrayList;

public class Presenter  implements PresenterView {


    private MainActivityView view;
    private InteractorView interactor;

    //metodo para comunicar la interfaz mainactivity con el interactor
    public Presenter(MainActivityView view, Context context) {
        this.view = view;
        interactor=new Interactor(this,context);
    }


    //metodo para obtener los datos

    @Override
    public void mostrarRick(ArrayList<Rick> tarjetasList) {
        view.mostrarRick(tarjetasList);
    }

    @Override
    public void getTarjeta() {
        interactor.getTarjeta();

    }

    @Override
    public void buscarTarjeta(String name) {
     interactor.buscarTarjeta(name);
    }


}
