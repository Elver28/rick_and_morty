package com.example.apiRick.Interfaces;

import com.example.apiRick.Modelo.Rick;

import java.util.ArrayList;

public interface PresenterView {
    void mostrarRick(ArrayList<Rick> tarjetasList);
    void getTarjeta();
    void buscarTarjeta(String name);
}
