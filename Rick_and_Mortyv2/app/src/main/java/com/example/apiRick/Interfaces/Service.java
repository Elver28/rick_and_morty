package com.example.apiRick.Interfaces;

import com.example.apiRick.Modelo.RickRespuesta;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Service {
    //obtiene las tarjetas para mostrarlas en el menu
    @GET("api/character")
    Call<RickRespuesta> obtenerListaRick();

    @GET("api/character/?")
    Call<RickRespuesta> obtenerBusqueda(@Query("name") String name);
}
