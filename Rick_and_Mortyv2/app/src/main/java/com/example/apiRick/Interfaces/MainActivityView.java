package com.example.apiRick.Interfaces;

import com.example.apiRick.Modelo.Rick;

import java.util.ArrayList;

public interface MainActivityView {
//almacena las tarjetas en una lista para luego mostrarlas
    void mostrarRick(ArrayList<Rick> tarjetasList);
    void buscarTarjeta(String name);
}
